class String
  VOWEL_REGEX = /[aeiou]/i
  def replace_vowel_by(arg)
    gsub(VOWEL_REGEX, arg)
  end
end
