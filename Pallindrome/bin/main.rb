require_relative '../lib/string'

EXIT_REGEX = /^q$/i

loop do
  puts 'Enter a string to check if it is pallindrome; enter \'q/Q\' to exit the program'
  input = gets.chomp
  if input =~ EXIT_REGEX
    puts 'Exit successful'
    break
  else
    puts input.pallindrome?
  end
end
