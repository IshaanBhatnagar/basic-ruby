class String

  ALPHABET_REGEX = /[a-z]/i

  def alphabet_occurence_count
    counting_hash = Hash.new(0)
    scan(ALPHABET_REGEX) { |key| counting_hash[key] += 1 }
    counting_hash
  end
end
