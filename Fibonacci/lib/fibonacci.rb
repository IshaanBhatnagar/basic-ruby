class Fibonacci
  def initialize(num1 = 0, num2 = 1)
    @num1 = num1
    @num2 = num2
  end

  def generate_series
    while @num1 <= 1000
      yield @num1
      @num1, @num2 = @num2, @num1 + @num2
    end
  end
end
