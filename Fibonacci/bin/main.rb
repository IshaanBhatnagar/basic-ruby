require_relative '../lib/fibonacci'

fibonacci = Fibonacci.new

fibonacci.generate_series { |num1| print num1, ' ' }
