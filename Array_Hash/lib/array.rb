class Array
  def group_by_length
    group_hash = Hash.new { |hash, key| hash[key] = [] }
    for element in self
      group_hash[element.to_s.length] << element
    end
    group_hash
  end
end
