require 'prime'

class Integer
  def self.generate_primes_upto(arg)
    1.step(arg, 2) { |n| print n, ' ' if n.prime? }
  end
end
