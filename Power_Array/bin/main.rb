require_relative '../lib/array'

puts 'Please enter an argument to generate a power array for [1, 2, 3, 4, 5, 6]'

arg = gets.chomp.to_i

p [1, 2, 3, 4, 5, 6].power(arg)
