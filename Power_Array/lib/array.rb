class Array
  def power(arg)
    map { |element| element ** arg }
  end
end
