class String

  LOWER_CASE = ('a'..'z')
  UP_CASE = ('A'..'Z')
  DIGITS = ('0'..'9')

  def character_type_count
    character_count = Hash.new(0)

    each_char do |char|
      case char
      when LOWER_CASE
        character_count[:lower_case] += 1
      when UP_CASE
        character_count[:up_case] += 1
      when DIGITS
        character_count[:digits] += 1
      else
        character_count[:special_character] += 1        
      end
    end
    character_count
  end

end