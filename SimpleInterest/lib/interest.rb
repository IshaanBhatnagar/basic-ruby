class Interest

  def initialize(principal, time, rate = 0.1, n = 1)
    @principal = principal
    @time = time
    @rate = rate
    @n = n
  end

  def simple_interest
    @principal * (1 + (@rate * @time))
  end

  def compound_interest
    @principal * ((1 + @rate / @n) ** (@n * @time))
  end

  def amount_difference
    p (compound_interest - simple_interest).round(2)
  end
end
