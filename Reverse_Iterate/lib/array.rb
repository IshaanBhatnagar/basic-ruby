class Array
  def reverse_iterate
    size = self.size
    while size > 0
      yield self[size - 1]
      size = size - 1
    end
  end
end
