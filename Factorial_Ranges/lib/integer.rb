class Integer
  def factorial
    if self < 0
      'Invalid entry, please input +ve integer'
    else
      (1..self).inject(1) { |memo, var| memo * var }
    end
  end
end
