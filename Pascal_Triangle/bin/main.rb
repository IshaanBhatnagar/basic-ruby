require_relative '../lib/pascal'

puts 'Enter argument for which Pascal Triangle is required'

arg = gets.chomp

pascal = Pascal.new

pascal.generate_triangle(arg)
