module Theorem
  def factorial(n)
    (1..n).inject(1) { |mem, var| mem * var }
  end

  def binomial(n,k)
    factorial(n) / (factorial(k) * factorial(n - k))
  end
end
