require_relative 'theorem'

class Pascal
  include Theorem

  def generate_triangle(row)
    for i in 0..row
      puts ((0..i).map { |e| binomial(i,e) }).join(' ')
    end
  end
end
