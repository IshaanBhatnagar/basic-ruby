class Array
  def group_by_length
    length_hash = Hash.new { |hash, key| hash[key] = [] }
    each { |key| length_hash[key.to_s.length] << key }
    length_hash.sort_by{ |k, v| k }.to_h
  end

  def group_by_parity
    group_by_length.inject(Hash.new { |hash, key| hash[key] = [] }) do |parity_hash, (key, value)|
      if key.even?
        parity_hash[:even] << value
      else
        parity_hash[:odd] << value
      end
      parity_hash
    end
  end
end
