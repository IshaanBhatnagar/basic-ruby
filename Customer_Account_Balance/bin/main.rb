require_relative '../lib/customer'

customer1 = Customer.new('Ishaan')
customer1.deposit(500)
puts customer1

customer2 = Customer.new('Udit')
customer2.withdraw(600)
puts customer2

customer3 = Customer.new('Paresh')
customer3.deposit(-50)
puts customer3

customer4 = Customer.new('Gaurav')
customer4.withdraw(100)
puts customer4

customer5 = Customer.new('Amit')
customer5.withdraw(-1100)
puts customer5
