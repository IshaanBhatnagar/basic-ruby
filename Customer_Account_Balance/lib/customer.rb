class Customer

  @@total_no_accounts = 0

  def initialize(name)
    @name = name
    @account_no = @@total_no_accounts + 1
    @@total_no_accounts += 1
    @balance = 1000
  end

  def deposit(credit)
    if credit <= 0
      puts 'Warning: Credit can not be 0 or less than 0'
    else
      @balance += credit
    end
  end

  def withdraw(debit)
    if debit > @balance
      puts 'Warning: Not enough balance'
    elsif debit <= 0
      puts 'Please input amount greater than 0'
    else      
      @balance -= debit
    end
  end

  def to_s
    "Customer: #{ @name }\n  Account no: #{ @account_no }\n  Balance: #{ @balance }"
  end
end
