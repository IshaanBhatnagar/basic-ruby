class String
  def highlight(search_string)
    occurence_count = 0
    new_string = gsub(/#{ search_string }/i) do |match|
    occurence_count += 1
    "(#{ match })" 
    end
  "#{new_string} \nTotal Occurences found : #{ occurence_count }"
  end
end
